import keras
from keras.layers import LSTM, Input, Dense, Dropout
from keras.layers.embeddings import Embedding
from keras.layers.wrappers import TimeDistributed
from keras.models import Sequential
from keras.optimizers import Adam
from keras.regularizers import l2

from utils import *


def build_model(input_size, num_categories, embedding_weights=None):
	model = Sequential()
	if LEARNING_RATE is not None:
		opt = Adam(lr=LEARNING_RATE, clipvalue=CLIP_GRAD)
	else:
		opt = Adam(clipvalue=CLIP_GRAD)
	kwargs = {
		"input_dim": num_categories,
		"input_length": input_size[1],
		"batch_input_shape": input_size,
	}
	if USE_WORDVEC:
		kwargs["output_dim"] = WORDVEC_LENGTH
	else:
		kwargs["output_dim"] = CHARVEC_LENGTH
	if embedding_weights is not None:
		kwargs["weights"] = [embedding_weights]
	
	e = Embedding(**kwargs)
	if FREEZE_EMBEDDING_WEIGHTS:
		e.trainable = False
	model.add(e)

	for i in range(NUM_LSTMs):
		kwargs = {
			"stateful": True,
			"activation": "tanh",
			"return_sequences": True,
			"W_regularizer": l2(LSTM_REG),
			"U_regularizer": l2(LSTM_REG),
			"b_regularizer": l2(LSTM_REG),
			"dropout_W": LSTM_DROPOUT_W,
			"dropout_U": LSTM_DROPOUT_U
		}
		lstm = LSTM(LSTM_HIDDEN, **kwargs)
		model.add(lstm)
		model.add(Dropout(LSTM_OUT_DROPOUT))
		
	if USE_DENSE:
		model.add(TimeDistributed(Dense(DENSE_UNITS, W_regularizer=l2(DENSE_REG), b_regularizer=l2(DENSE_REG))))
		model.add(Dropout(DENSE_DROPOUT))
	if USE_WORDVEC:
		model.add(TimeDistributed(Dense(WORDVEC_LENGTH, activation="tanh", W_regularizer=l2(DENSE_REG))))
		model.compile(loss="mean_squared_error", optimizer=opt)
	else:
		model.add(TimeDistributed(Dense(num_categories, activation="softmax", W_regularizer=l2(DENSE_REG))))
		model.compile(loss="categorical_crossentropy", metrics=["accuracy"], optimizer=opt)
	return model


def train_model(model, X, Y, wordvec=None, start_from=None):
	epoch_num = 1
	if start_from is not None:
		epoch_num = start_from
	while True:
		stop = False
		for i in range(len(X)):
			print("EPOCH", epoch_num)
			try:
				tmp_num = len(X[i])//BATCH_SIZE
				spl = int(tmp_num*VAL_SPLIT)/tmp_num
				model.fit(X[i], Y[i], batch_size=BATCH_SIZE, nb_epoch=1, shuffle=False, verbose=1, validation_split=spl)
				if USE_WORDVEC and epoch_num % 250 == 0:
					y = model.predict(X[i], batch_size=BATCH_SIZE, verbose=1)
					num_correct = total = 0
					for batch_num in range(len(y)):
						for seq_num in range(len(y[batch_num])):
							pred_word = wordvec.similar_by_vector(y[batch_num][seq_num], topn=1)[0][0]
							word = wordvec.similar_by_vector(Y[i][batch_num][seq_num], topn=1)[0][0]
							if pred_word == word:
								num_correct += 1
							total += 1
					print("ACC:", num_correct/total)
			except KeyboardInterrupt:
				stop = True
				break
			reset_lstm(model)
			if epoch_num >= NUM_EPOCHS:
				stop = True
				break
			if epoch_num % SAVE_EVERY == 0:
				model.save_weights(WEIGHTS_DIR.format(epoch_num))
			epoch_num += 1
		if stop:
			break
	reset_lstm(model)
	model.save_weights(WEIGHTS_DIR.format(epoch_num))


if __name__ == "__main__":
	setup()
	names, train = read_from_dir("train", IGNORE_CAPITALIZATION)
	w = None
	if not USE_WORDVEC:
		mapping, _ = get_char_mappings(concat_all())
		print("Training (CharVec):", names)
	else:
		w = get_wordvec(concat_all())
		print("Training (WordVec):", names)
	X = []
	Y = []
	for text in train:
		if USE_WORDVEC:
			x, y = get_wordvec_data(text, w)
		else:
			x, y = to_chunked(list(map(mapping.get, text)), len(mapping), to_categorical=True)
		X.append(x)
		Y.append(y)
	if not USE_WORDVEC:
		model = build_model((BATCH_SIZE, SEQ_LENGTH), len(mapping))
	else:
		model = build_model((BATCH_SIZE, SEQ_LENGTH), len(w.vocab), embedding_weights=get_wordvec_embedding_weights(w))
	start_from = None
	if CONTINUE_TRAINING_EPOCH is not None:
		model.load_weights(WEIGHTS_DIR.format(CONTINUE_TRAINING_EPOCH))
		start_from = CONTINUE_TRAINING_EPOCH + 1
	train_model(model, X, Y, w, start_from=start_from)
