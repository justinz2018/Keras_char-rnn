import random, glob, os, sys

import numpy as np
import keras
from keras.utils import np_utils
import nltk
from gensim.models.word2vec import Word2Vec

def read_from_dir(directory, ignore_capitalization):
	files = sorted(glob.glob(directory+"/**.txt", recursive=True))
	random.shuffle(files)
	ret = []
	names = []
	for fi in files:
		f = open(fi, "r")
		text = f.read().strip()
		if ignore_capitalization:
			text = text.lower()
		ret.append(text)
		names.append(os.path.splitext(os.path.basename(fi))[0])
		f.close()
	return (names, ret)

from settings import *

def reset_lstm(model):
	for my_layer in model.layers:
		if type(my_layer) == keras.layers.recurrent.LSTM:
			my_layer.reset_states()

def setup():
	sys.setrecursionlimit(10000)
	random.seed(RNG_SEED)
	np.random.seed(random.randint(0, 4294967295))

def to_chunked(x, mapping_len=None, seq_length=SEQ_LENGTH, batch_size=BATCH_SIZE, to_categorical=True):
	tmp_X = x[:-1]
	tmp_Y = x[1:]
	chunked_X = [tmp_X[i:i+seq_length] for i in range(0, len(tmp_X), seq_length)]
	if to_categorical:
		tmp_Y = np_utils.to_categorical(tmp_Y, mapping_len)
	chunked_Y = [tmp_Y[i:i+seq_length] for i in range(0, len(tmp_Y), seq_length)]
	if len(chunked_X[-1]) != seq_length:
		chunked_X = chunked_X[:-1]
	if len(chunked_Y[-1]) != seq_length:
		chunked_Y = chunked_Y[:-1]
	if len(chunked_X) % batch_size:
		chunked_X = chunked_X[:-(len(chunked_X)%batch_size)]
	if len(chunked_Y) % batch_size:
		chunked_Y = chunked_Y[:-(len(chunked_Y)%batch_size)]
	return (np.asarray(chunked_X), np.asarray(chunked_Y))

def concat_all():
	_, train = read_from_dir("train", IGNORE_CAPITALIZATION)
	return "\n".join(train+[x[1] for x in TESTS])

def get_char_mappings(text):
	chars = set(text)
	mapping = dict(list(map(reversed, enumerate(sorted(chars)))))	#char -> int
	reverse_mapping = {v:k for k,v in mapping.items()}		#int -> char
	return (mapping, reverse_mapping)

def get_wordvec(text, size=WORDVEC_LENGTH, window=WORDVEC_WINDOW):
	sents = nltk.sent_tokenize(text)
	for i in range(len(sents)):
		sents[i] = nltk.word_tokenize(sents[i])
	w = Word2Vec(sents, size=WORDVEC_LENGTH, window=WORDVEC_WINDOW, min_count=0)
	w.init_sims(replace=True)
	return w

def get_wordvec_mappings(wordvec):
	mapping = dict(list(map(reversed, enumerate(wordvec.index2word))))	#word -> int
	reverse_mapping = {v:k for k,v in mapping.items()}	#int -> word
	return (mapping, reverse_mapping)
	

def get_wordvec_embedding_weights(wordvec):
	weights = np.zeros((len(wordvec.index2word), len(wordvec[wordvec.index2word[0]])))
	for i, word in enumerate(wordvec.index2word):
		weights[i, :] = wordvec[word]
	return weights

def get_wordvec_data(text, wordvec, seq_length=SEQ_LENGTH, batch_size=BATCH_SIZE):
	mapping, _ = get_wordvec_mappings(wordvec)
	text = nltk.word_tokenize(text)
	x, y_tmp = to_chunked(list(map(mapping.get, text)), None, to_categorical=False,
		seq_length=seq_length, batch_size=batch_size)
	y = np.zeros((y_tmp.shape[0], y_tmp.shape[1], WORDVEC_LENGTH))
	for i in range(len(y_tmp)):
		for j in range(len(y_tmp[i])):
			y[i, j, :] = wordvec[wordvec.index2word[y_tmp[i, j]]]
	return (x, y)
