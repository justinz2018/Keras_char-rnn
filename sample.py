import glob, os, random

import numpy as np

from train import build_model
from utils import *

def sample(pk, temperature=TEMPERATURE):
	pk = np.asarray(pk).astype("float64")
	pk = np.log(pk) / temperature
	exp_pk = np.exp(pk)
	pk = exp_pk / np.sum(exp_pk)
	return np.argmax(np.random.multinomial(1, pk, 1))
	

def sample_from_text(model, name, text, mapping=None, reverse_mapping=None,
	temperature=TEMPERATURE, num_chars_gen=NUM_CHARS_GEN, wordvec=None
	):
	print(name, "------------------------------")
	reset_lstm(model)
	current = np.zeros((1, 1))
	if len(text):
		if USE_WORDVEC:
			x, y = get_wordvec_data(text, wordvec, 1, 1)
		else:
			x, y = to_chunked(list(map(mapping.get, text)), len(mapping), 1, 1)
		#model.fit(x, y, batch_size=1, nb_epoch=1, shuffle=False, verbose=1)
		print(model.evaluate(x, y, batch_size=1, verbose=1))
		current[0, 0] = x[-1][-1]
	else:
		if USE_WORDVEC:
			current[0, 0] = random.choice(list(range(len(wordvec.index2word))))
		else:
			current[0, 0] = random.choice(list(reverse_mapping))
	if USE_WORDVEC:
		wordvec_reverse_mapping = dict(map(reversed, enumerate(wordvec.index2word)))
	gen = ""
	for i2 in range(num_chars_gen):
		try:
			pk = model.predict(current, verbose=0, batch_size=1)[0][0]
			if USE_WORDVEC:
				if i2 % 1000 == 0:
					print(pk)
				word = wordvec.similar_by_vector(pk, topn=len(WORDVEC_RANDOM_SAMPLE))
				idx = np.argmax(np.random.multinomial(1, WORDVEC_RANDOM_SAMPLE, 1))
				word = word[idx][0]
				if i2 % 1000 == 0:
					print(wordvec.similar_by_vector(pk))
				gen += word+" "
				current[0, 0] = wordvec_reverse_mapping[word]
			else:
				c = sample(pk, temperature)
				if c in reverse_mapping:
					gen += reverse_mapping[c]
				current[0, 0] = c
			if i2 % 1000 == 0:
				print("{}/{}".format(i2, num_chars_gen))
		except KeyboardInterrupt:
			break
	return gen

if __name__ == "__main__":
	setup()
	assert abs(sum(WORDVEC_RANDOM_SAMPLE)-1) <= np.finfo(float).eps, "WORDVEC_RANDOM_SAMPLE must sum to 1"
	if not USE_WORDVEC:
		mapping, reverse_mapping = get_char_mappings(concat_all())
		test_model = build_model((1, 1), len(mapping))
	else:
		w = get_wordvec(concat_all())
		test_model = build_model((1, 1), len(w.vocab))
	# Patched /usr/local/lib/python3.5/dist-packages/Keras-1.0.7-py3.5.egg/keras/engine/training.py line 192:
	# `if y.shape[1] == 1:` -> `if y.shape[-1] == 1:`
	# to make this work
	
	if LOAD_MODEL_EPOCH is not None:
		filepath = WEIGHTS_DIR.format(LOAD_MODEL_EPOCH)
	else:
		filepath = sorted(glob.glob(WEIGHTS_DIR.replace("{}", "*")), key=lambda x: (len(x), x))[-1]
	
	test_model.load_weights(filepath)
	print("Loaded weights from", filepath)
	
	for name, text in TESTS:
		if USE_WORDVEC:
			gen = sample_from_text(test_model, name, text, wordvec=w)
		else:
			gen = sample_from_text(test_model, name, text, mapping=mapping, reverse_mapping=reverse_mapping)
		f = open("output/{}.txt".format(name), "w")
		f.write(gen)
		f.close()
	if len(TESTS) == 0:
		name = "default"
		if USE_WORDVEC:
			gen = sample_from_text(test_model, name, text, wordvec=w)
		else:
			gen = sample_from_text(test_model, name, text, mapping=mapping, reverse_mapping=reverse_mapping)
		f = open("output/{}.txt".format(name), "w")
		f.write(gen)
		f.close()
		
