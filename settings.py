from utils import read_from_dir

RNG_SEED = 1417048

LSTM_HIDDEN = 512
NUM_CHARS_GEN = 2000
NUM_EPOCHS = 250
TEMPERATURE = 1	# TEMPERATURE in (0, 1], ~0 = more confident, ~1 = more randomness
VAL_SPLIT = .05

CLIP_GRAD = 5.0
LSTM_REG = .0
LSTM_OUT_DROPOUT = .0
LSTM_DROPOUT_W = .4
LSTM_DROPOUT_U = .1
NUM_LSTMs = 3
LEARNING_RATE = .005 # None = default (0.001)

USE_DENSE = False
DENSE_DROPOUT = .5
DENSE_UNITS = 512
DENSE_REG = .000

CHARVEC_LENGTH = 80
BATCH_SIZE = 64
SEQ_LENGTH = 64

USE_WORDVEC = True # False = use char vec
WORDVEC_LENGTH = 100
WORDVEC_WINDOW = 5
CONTINUE_TRAINING_EPOCH = 180 # None = restart training

IGNORE_CAPITALIZATION = USE_WORDVEC # Wordvec is more sensitive to capitalization
FREEZE_EMBEDDING_WEIGHTS = USE_WORDVEC

SAVE_EVERY = 10
WEIGHTS_DIR = "model_bkup/{}.h5" # {} = num_epoch
LOAD_MODEL_EPOCH = None # None = highest one
WORDVEC_RANDOM_SAMPLE = [.5, .25, .125, .075, .05]
TESTS = [
	# (NAME, TEST TEXT)
]
if len(TESTS) == 0:
	TESTS = list(zip(*read_from_dir("test", IGNORE_CAPITALIZATION)))
if IGNORE_CAPITALIZATION:
	TESTS = [(x, y.lower()) for x, y in TESTS]
